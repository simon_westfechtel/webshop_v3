﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webshop_v3.Models
{
    public class Menu
    {
        public Menu () {
            Produkte = new Dictionary < string , Produkt > ();
        }
        public DateTime Tag { get; set; }
        public int KalenderWoche { get; set; }
        public string Motto { get; set; }
        public Dictionary <string,Produkt> Produkte { get; set; }
        public Bild Highlight { get; set; }
    }
}