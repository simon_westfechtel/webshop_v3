﻿namespace webshop_v3.Models {
    public class Bild {
        public Bild ( int id , string alttext , string titel , string unterschrift , string binaerdaten ,
            int kategorie , int produkt ) {
            Id = id;
            AltText = alttext;
            Titel = titel;
            Unterschrift = unterschrift;
            Binaerdaten = binaerdaten;
            Kategorie = kategorie;
            Produkt = produkt;
        }

        public int Id { get; set; }
        public string AltText { get; set; }
        public string Titel { get; set; }
        public string Unterschrift { get; set; }
        public string Binaerdaten { get; set; }
        public int Kategorie { get; set; }
        public int Produkt { get; set; }

        public override bool Equals ( object obj ) {
            if ( obj == null ) return false;
            var objAsBild = obj as Bild;
            if ( objAsBild == null ) return false;
            return Equals ( objAsBild );
        }

        public override int GetHashCode () {
            return Id;
        }

        public bool Equals ( Bild other ) {
            if ( other == null ) return false;
            return Id.Equals ( other.Id );
        }
    }
}