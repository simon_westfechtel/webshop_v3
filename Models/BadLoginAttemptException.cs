﻿using System;

namespace webshop_v3.Models {
    public class BadLoginAttemptException : Exception {
        public BadLoginAttemptException ( string message ) : base ( message ) {
        }
    }
}