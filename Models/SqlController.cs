﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using PasswordSecurity;

namespace webshop_v3.Models {
    public class SqlController {
        private readonly MySqlConnection connection;

        public SqlController () {
            connection = new MySqlConnection ( ConfigurationManager.ConnectionStrings ["DbConStr"].ConnectionString );
            try {
                connection.Open ();
            }
            catch ( Exception e ) {
            }
        }

        public List < Produkt > GetProdukte () {
            var produkte = new List < Produkt > ();
            var command = connection.CreateCommand ();
            command.CommandText = "SELECT * FROM Produkt";
            var reader = command.ExecuteReader ();
            while ( reader.Read () ) {
                var produkt = new Produkt ( reader.GetInt32 ( "ID" ) , reader.GetString ( "Beschreibung" ) ,
                    reader.GetString ( "Name" ) , reader.GetInt32 ( "Kategorie" ) );
                produkte.Add ( produkt );
            }
            reader.Close ();
            return produkte;
        }

        public List < Bild > GetBilder () {
            var bilder = new List < Bild > ();
            var command = connection.CreateCommand ();
            command.CommandText = "SELECT * FROM Bild";
            var reader = command.ExecuteReader ();
            while ( reader.Read () ) {
                //Bild bild = new Bild((int)reader["ID"], (string)reader["AltText"], (string)reader["Title"], (string)reader["Unterschrift"], Convert.ToBase64String((byte[])reader["Binärdaten"]));
                var bild = new Bild (
                    reader.GetInt32 ( "ID" ) , reader.GetString ( "AltText" ) , reader.GetString ( "Title" ) ,
                    reader.GetString ( "Unterschrift" ) ,
                    Convert.ToBase64String ( ( byte [ ] ) reader ["Binaerdaten"] ) ,
                    reader.GetInt32 ( "Kategorie" ) , reader.GetInt32 ( "Produkt" ) );
                bilder.Add ( bild );
            }
            reader.Close ();
            return bilder;
        }

        public List < Kategorie > GetOberkategorien () {
            var kategorien = new List < Kategorie > ();
            var command = connection.CreateCommand ();
            command.CommandText = "SELECT * FROM Kategorie WHERE Oberkategorie = ID";
            var reader = command.ExecuteReader ();
            while ( reader.Read () ) {
                var kategorie = new Kategorie ( reader.GetInt32 ( "ID" ) , reader.GetString ( "Bezeichnung" ) ,
                    reader.GetInt32 ( "Oberkategorie" ) );
                kategorien.Add ( kategorie );
            }
            reader.Close ();
            return kategorien;
        }

        public List < Kategorie > GetUnterkategorien () {
            var kategorien = new List < Kategorie > ();
            var command = connection.CreateCommand ();
            command.CommandText = "SELECT * FROM Kategorie WHERE Oberkategorie != ID";
            var reader = command.ExecuteReader ();
            while ( reader.Read () ) {
                var kategorie = new Kategorie ( reader.GetInt32 ( "ID" ) , reader.GetString ( "Bezeichnung" ) ,
                    reader.GetInt32 ( "Oberkategorie" ) );
                kategorien.Add ( kategorie );
            }
            reader.Close ();
            return kategorien;
        }

        public List < int > GetStudenten () {
            var studenten = new List < int > ();
            var command = connection.CreateCommand ();
            command.CommandText = "SELECT NR FROM student WHERE 1";
            var reader = command.ExecuteReader ();
            while ( reader.Read () ) studenten.Add ( reader.GetInt32 ( "NR" ) );
            reader.Close ();
            return studenten;
        }

        public List < int > GetMitarbeiter () {
            var mitarbeiter = new List < int > ();
            var command = connection.CreateCommand ();
            command.CommandText = "SELECT NR FROM mitarbeiter WHERE 1";
            var reader = command.ExecuteReader ();
            while ( reader.Read () )
                mitarbeiter.Add ( reader.GetInt32 ( "NR" ) );
            reader.Close ();
            return mitarbeiter;
        }

        public List < int > GetGaeste () {
            var gaeste = new List < int > ();
            var command = connection.CreateCommand ();
            command.CommandText = "SELECT NR FROM gast WHERE 1";
            var reader = command.ExecuteReader ();
            while ( reader.Read () )
                gaeste.Add ( reader.GetInt32 ( "NR" ) );
            reader.Close ();
            return gaeste;
        }

        public string GetRole ( int id ) {
            var studenten = GetStudenten ();
            var mitarbeiter = GetMitarbeiter ();
            var gaeste = GetGaeste ();

            var command = connection.CreateCommand ();
            var query = string.Format ( "SELECT NR FROM FE_Nutzer WHERE NR = {0}" , id );
            command.CommandText = query;
            var reader = command.ExecuteReader ();
            reader.Read ();

            if ( mitarbeiter.Contains ( reader.GetInt32 ( "NR" ) ) ) return "Mitarbeiter";
            if ( gaeste.Contains ( reader.GetInt32 ( "NR" ) ) ) return "Gast";
            if ( studenten.Contains ( reader.GetInt32 ( "NR" ) ) ) return "Student";
            return "Backend-Nutzer";
        }

        public User LoginUser ( string usr , string pass ) {
            //var command = connection.CreateCommand ();
            //var query = string.Format ( "SELECT Auth_id FROM FE_Nutzer WHERE Loginname = \'{0}\'" , usr );
            //command.CommandText = query;
            //var reader = command.ExecuteReader ();
            //if ( !reader.HasRows )
            //    throw new BadLoginAttemptException ( "Login fehlgeschlagen. Überprüfen Sie Ihren Loginnamen." );
            //reader.Read ();
            //var auth_id = reader.GetInt32 ( "Auth_id" );
            //reader.Close ();

            //query = string.Format ( "SELECT * FROM Auth_type WHERE ID = {0}" , auth_id );
            //command.CommandText = query;
            //reader = command.ExecuteReader ();
            //reader.Read ();
            //var verify_hash = reader.GetString ( "hash" );
            //reader.Close ();
            var command = connection.CreateCommand ();
            var query = string.Format ( "SELECT Passwort, Aktiv FROM FE_Nutzer WHERE Loginname = \'{0}\'" , usr );
            command.CommandText = query;
            var reader = command.ExecuteReader ();
            if ( !reader.HasRows )
                throw new BadLoginAttemptException ( "Login fehlgeschlagen. Überprüfen Sie Ihren Loginnamen." );
            reader.Read ();

            if ( !reader.GetBoolean ( "Aktiv" ) )
                throw new BadLoginAttemptException ( "Login fehlgeschlagen. Ihr Account ist inaktiv." );

            var verify_hash = reader.GetString ( "Passwort" );
            reader.Close ();

            if ( PasswordStorage.VerifyPassword ( pass , verify_hash ) ) {
// password good
                var studenten = GetStudenten ();
                var mitarbeiter = GetMitarbeiter ();
                var gaeste = GetGaeste ();
                var role = "BackendNutzer";


                query = string.Format ( "SELECT * FROM FE_Nutzer WHERE Loginname = \'{0}\'" , usr );
                command.CommandText = query;
                reader = command.ExecuteReader ();
                reader.Read ();


                if ( mitarbeiter.Contains ( reader.GetInt32 ( "NR" ) ) ) role = "Mitarbeiter";
                if ( gaeste.Contains ( reader.GetInt32 ( "NR" ) ) ) role = "Gast";
                if ( studenten.Contains ( reader.GetInt32 ( "NR" ) ) ) role = "Student";

                var user = new User ( reader.GetInt32 ( "NR" ) , reader.GetString ( "Loginname" ) ,
                    reader.GetString ( "Email" ) ,
                    reader.GetBoolean ( "Aktiv" ) , reader.GetString ( "LetzterLogin" ) ,
                    reader.GetString ( "Vorname" ) ,
                    reader.GetString ( "Nachname" ) , role );
                reader.Close ();

                query = string.Format ( "update fe_nutzer set LetzterLogin=\'{0}\' where Loginname=\'{1}\'" ,
                    DateTime.Today.ToString ( "yyyy-MM-dd" ) , usr );
                command.CommandText = query;
                command.ExecuteNonQuery ();


                return user;
            }
            throw new BadLoginAttemptException ( "Login fehlgeschlagen. Überprüfen Sie Ihr Passwort." );
        }

        /*public void RegisterUser ( string usr , string pass ) {
            var command = connection.CreateCommand ();
            string query = "";
            query = String.Format ( "INSERT INTO Auth_type (hash,salt,stretch,algo) VALUES (\'{0}\',\'\',0,\'sha256\')" ,
                PasswordSecurity.PasswordStorage.CreateHash ( pass ) );
            command.CommandText = query;
            command.ExecuteNonQuery ();

            query = String.Format("insert into fe_nutzer (Auth_id, Loginname, Email, Aktiv, Anlegedatum, LetzerLogin, Bestellung, Vorname, Nachname) " +
                                  "values (1, \'{0}\', \'mailsimonwest\', true, \'2017-12-08\', \'2017-12-08\', null, \'Simon\', \'Westfechtel\') ", usr);
            command.CommandText = query;
            command.ExecuteNonQuery();

            query = String.Format("insert into fh_angehörige (NR) values (1)");
            command.CommandText = query;
            command.ExecuteNonQuery();

            query = String.Format("INSERT INTO Student (NR, Matrikelnummer, Studiengang) VALUES (1, 353822, \'Informatik\')");
            command.CommandText = query;
            command.ExecuteNonQuery();
        }*/
        public void RegisterUser ( RegistrierenListe registrierenListe ) {
            var tr = connection.BeginTransaction ();
            var command = connection.CreateCommand ();
            command.Transaction = tr;
            var query = "";
            var password = PasswordStorage.CreateHash ( registrierenListe.Passwort );

            switch ( registrierenListe.Type ) {
                case 1: //gast


                    try {
                        //query = String.Format (
                        //    "INSERT INTO Auth_type (hash,salt,stretch,algo) VALUES (\'{0}\',\'\',0,\'sha256\')" ,
                        //     password);
                        //command.CommandText = query;
                        //command.ExecuteNonQuery ();

                        //query = String.Format ( "SELECT ID FROM Auth_type WHERE hash = \'{0}\'" ,
                        //    PasswordStorage.CreateHash ( registrierenListe.Passwort ) );
                        //command.CommandText = query;
                        //var reader = command.ExecuteReader ();
                        //reader.Read ();
                        //var auth_id = reader.GetInt32 ( "ID" );
                        //reader.Close ();

                        query = "SELECT NR FROM fe_nutzer WHERE 1";
                        command.CommandText = query;
                        var reader = command.ExecuteReader ();
                        int max_id = 0 , new_id = 0;
                        while ( reader.Read () ) if ( ( new_id = reader.GetInt32 ( "NR" ) ) > max_id ) max_id = new_id;
                        max_id++;
                        reader.Close ();

                        query = string.Format (
                            "insert into fe_nutzer (NR, Passwort, Loginname, Email, Aktiv, Anlegedatum, LetzterLogin, Bestellung, Vorname, Nachname) " +
                            "values ({0}, \'{1}\', \'{2}\', \'{3}\', false, \'{4}\', \'0001-01-01\', null, \'{5}\', \'{6}\') " ,
                            max_id , password , registrierenListe.Loginname , registrierenListe.Email ,
                            registrierenListe.Datum ,
                            registrierenListe.Vorname , registrierenListe.Nachname );
                        command.CommandText = query;
                        command.ExecuteNonQuery ();


                        query = string.Format (
                            "insert into gast (NR, Grund, Ablauf) values ({0},\'{1}\',\'0001-01-01\')" , max_id ,
                            registrierenListe.Grund );
                        command.CommandText = query;
                        command.ExecuteNonQuery ();
                        tr.Commit ();
                    }
                    catch ( Exception e ) {
                        try {
                            tr.Rollback ();
                            Console.WriteLine ( "Exception of type " + e.GetType () +
                                                "encountered while trying to insert data. No record written to database." );
                        }
                        catch ( SqlException ex ) {
                            if ( tr.Connection != null )
                                Console.WriteLine ( "Exception of type " + ex.GetType () +
                                                    "encountered while trying to roll back." );
                        }
                    }


                    break;
                case 2: //student


                    try {
                        //query = String.Format (
                        //    "INSERT INTO Auth_type (hash,salt,stretch,algo) VALUES (\'{0}\',\'\',0,\'sha256\')" ,
                        //    password );
                        //command.CommandText = query;
                        //command.ExecuteNonQuery ();

                        //query = String.Format ( "SELECT ID FROM Auth_type WHERE hash = \'{0}\'" ,
                        //    PasswordStorage.CreateHash ( registrierenListe.Passwort ) );
                        //command.CommandText = query;
                        //var reader = command.ExecuteReader ();
                        //reader.Read ();
                        //var auth_id = reader.GetInt32 ( "ID" );
                        //reader.Close ();

                        query = "SELECT NR FROM fe_nutzer WHERE 1";
                        command.CommandText = query;
                        var reader = command.ExecuteReader ();
                        int max_id = 0 , new_id = 0;
                        while ( reader.Read () )
                            if ( ( new_id = reader.GetInt32 ( "NR" ) ) > max_id )
                                max_id = new_id;
                        max_id++;
                        reader.Close ();

                        query = string.Format (
                            "insert into fe_nutzer (NR, Passwort, Loginname, Email, Aktiv, Anlegedatum, LetzterLogin, Bestellung, Vorname, Nachname) " +
                            "values ({0}, \'{1}\', \'{2}\', \'{3}\', false, \'{4}\', \'0001-01-01\', null, \'{5}\', \'{6}\') " ,
                            max_id , password , registrierenListe.Loginname , registrierenListe.Email ,
                            registrierenListe.Datum ,
                            registrierenListe.Vorname , registrierenListe.Nachname );
                        command.CommandText = query;
                        command.ExecuteNonQuery ();

                        query = string.Format (
                            "insert into fh_angehörige (NR) values ({0})" , max_id );
                        command.CommandText = query;
                        command.ExecuteNonQuery ();

                        query = string.Format (
                            "insert into student (NR,Matrikelnummer,Studiengang) values ({0},{1},\'{2}\')" , max_id ,
                            int.Parse ( registrierenListe.MatrikelNummer ) , registrierenListe.StudienGang );
                        command.CommandText = query;
                        command.ExecuteNonQuery ();

                        tr.Commit ();
                    }
                    catch ( Exception e ) {
                        try {
                            tr.Rollback ();
                            Console.WriteLine ( "Exception of type " + e.GetType () +
                                                "encountered while trying to insert data. No record written to database." );
                        }
                        catch ( SqlException ex ) {
                            if ( tr.Connection != null )
                                Console.WriteLine ( "Exception of type " + ex.GetType () +
                                                    "encountered while trying to roll back." );
                        }
                    }
                    break;
                case 3: //mitarbeiter
                    try {
                        //query = String.Format (
                        //    "INSERT INTO Auth_type (hash,salt,stretch,algo) VALUES (\'{0}\',\'\',0,\'sha256\')" ,
                        //    password );
                        //command.CommandText = query;
                        //command.ExecuteNonQuery ();

                        //query = String.Format ( "SELECT ID FROM Auth_type WHERE hash = \'{0}\'" ,
                        //    PasswordStorage.CreateHash ( registrierenListe.Passwort ) );
                        //command.CommandText = query;
                        //var reader = command.ExecuteReader ();
                        //reader.Read ();
                        //var auth_id = reader.GetInt32 ( "ID" );
                        //reader.Close ();

                        query = "SELECT NR FROM fe_nutzer WHERE 1";
                        command.CommandText = query;
                        var reader = command.ExecuteReader ();
                        int max_id = 0 , new_id = 0;
                        while ( reader.Read () )
                            if ( ( new_id = reader.GetInt32 ( "NR" ) ) > max_id )
                                max_id = new_id;
                        max_id++;
                        reader.Close ();

                        query = string.Format (
                            "insert into fe_nutzer (NR, Passwort, Loginname, Email, Aktiv, Anlegedatum, LetzterLogin, Bestellung, Vorname, Nachname) " +
                            "values ({0}, \'{1}\', \'{2}\', \'{3}\', false, \'{4}\', \'0001-01-01\', null, \'{5}\', \'{6}\') " ,
                            max_id , password , registrierenListe.Loginname , registrierenListe.Email ,
                            registrierenListe.Datum ,
                            registrierenListe.Vorname , registrierenListe.Nachname );
                        command.CommandText = query;
                        command.ExecuteNonQuery ();

                        query = string.Format (
                            "insert into fh_angehörige (NR) values ({0})" , max_id );
                        command.CommandText = query;
                        command.ExecuteNonQuery ();

                        query = string.Format (
                            "insert into mitarbeiter (NR,MA_Nummer,Telefonnummer,Büro) values ({0},\'{1}\',\'{2}\',\'{3}\')" ,
                            max_id , registrierenListe.MitarbeiterNummer , registrierenListe.TelefonNummer ,
                            registrierenListe.Buero );
                        command.CommandText = query;
                        command.ExecuteNonQuery ();

                        tr.Commit ();
                    }
                    catch ( Exception e ) {
                        try {
                            tr.Rollback ();
                            Console.WriteLine ( "Exception of type " + e.GetType () +
                                                "encountered while trying to insert data. No record written to database." );
                        }
                        catch ( SqlException ex ) {
                            if ( tr.Connection != null )
                                Console.WriteLine ( "Exception of type " + ex.GetType () +
                                                    "encountered while trying to roll back." );
                        }
                    }
                    break;
            }
        }

        public Preis GetProduktPreis ( int produktId ) {
            var command = connection.CreateCommand ();
            var query = "";
            query = string.Format ( "SELECT * FROM preis WHERE Produkt = {0}" , produktId );
            command.CommandText = query;
            var reader = command.ExecuteReader ();
            reader.Read ();
            var preis = new Preis ( reader.GetDouble ( "Studentenbetrag" ) , reader.GetDouble ( "Gastbetrag" ) ,
                reader.GetDouble ( "Mitarbeiterbetrag" ) , reader.GetInt32 ( "Produkt" ) );
            reader.Close ();
            return preis;
        }

        public bool CheckUserEmail ( string email ) {
            var command = connection.CreateCommand ();
            var query = "";
            query = string.Format ( "SELECT * FROM fe_nutzer WHERE Email = \'{0}\'" , email );
            command.CommandText = query;
            var reader = command.ExecuteReader ();
            var res = reader.HasRows;
            reader.Close ();
            return res;
        }

        public bool CheckUserNick ( string username ) {
            var command = connection.CreateCommand ();
            var query = "";
            query = string.Format ( "SELECT * FROM fe_nutzer WHERE Loginname = \'{0}\'" , username );
            command.CommandText = query;
            var reader = command.ExecuteReader ();
            var res = reader.HasRows;
            reader.Close ();
            return res;
        }

        public void ActivateUser ( int id ) {
            var command = connection.CreateCommand ();
            var query = string.Format("update fe_nutzer set Aktiv=1 where NR={0}",
                id);
            command.CommandText = query;
            command.ExecuteNonQuery();
        }

        public void DeactivateUser(int id)
        {
            var command = connection.CreateCommand();
            var query = string.Format("update fe_nutzer set Aktiv=0 where NR={0}",
                id);
            command.CommandText = query;
            command.ExecuteNonQuery();
        }
    }
}