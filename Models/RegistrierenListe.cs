﻿namespace webshop_v3.Models {
    public class RegistrierenListe {
        public int Type { get; set; }
        public string Loginname { get; set; }
        public string Email { get; set; }
        public string Datum { get; set; }
        public string Passwort { get; set; }
        public string Vorname { get; set; }
        public string Nachname { get; set; }
        public string Grund { get; set; }
        public string MitarbeiterNummer { get; set; }
        public string TelefonNummer { get; set; }
        public string Buero { get; set; }
        public string MatrikelNummer { get; set; }
        public string StudienGang { get; set; }
    }
}