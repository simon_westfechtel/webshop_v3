﻿namespace webshop_v3.Models {
    public class Preis {
        public Preis ( double studentenpreis , double gastpreis , double mitarbeiterpreis , double produkt ) {
            Studentenpreis = studentenpreis;
            Gastpreis = gastpreis;
            Mitarbeiterpreis = mitarbeiterpreis;
            Produkt = produkt;
        }

        public double Studentenpreis { get; set; }
        public double Gastpreis { get; set; }
        public double Mitarbeiterpreis { get; set; }
        public double Produkt { get; set; }
    }
}