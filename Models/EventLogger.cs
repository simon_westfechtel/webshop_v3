﻿using System.IO;

namespace webshop_v3.Models {
    public class EventLogger {
        private readonly StreamWriter streamWriter;

        public EventLogger ( string file ) {
            if ( File.Exists ( file ) )
                File.Delete ( file );
            streamWriter = File.CreateText ( file );
        }

        public void write ( string text ) {
            streamWriter.WriteLine ( text );
        }
    }
}