﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using LinqToDB.Common;

namespace webshop_v3.Models
{
    public class XmlController
    {
        public XmlController () {
            
        }

        public Menu GetMenu () {
            Menu menu = new Menu ();
            SqlController sqlController = new SqlController ();
            try {
                string file = HttpRuntime.AppDomainAppPath + "\\resources\\Speiseplan.xml";
                var document = XDocument.Load ( file );
                string test = DateTime.Today.ToString ( "s" ).Substring ( 0 , 10 );
                var query = from d in document.Root.Descendants ( "Menu" )
                    where d.Attribute ( "Tag" ).Value.Substring ( 0,10 ) == DateTime.Today.ToString ( "s" ).Substring ( 0 , 10 )
                    select d;

                var today = query.FirstOrDefault ();
                menu.Motto = today.Element ( "Motto" ).Value;
                menu.KalenderWoche = Int32.Parse(today.Attribute ( "Kalenderwoche" ).Value);
                menu.Tag = DateTime.Parse(today.Attribute ( "Tag" ).Value);

                query = from d in today.Descendants ( "Produkt" )
                    select d;
                foreach ( var produkt in query ) {
                    Produkt p = sqlController.GetProdukte ()
                        .Find ( x => x.Id == Int32.Parse ( produkt.Attribute ( "ProduktID" ).Value ) );
                    if(p != null) menu.Produkte.Add ( produkt.Attribute ( "Typ" ).Value, p );
                }
                return menu;
            }
            catch ( Exception e ) {
                Console.WriteLine(e.ToString ());
            }
            return null;
        }
    }
}