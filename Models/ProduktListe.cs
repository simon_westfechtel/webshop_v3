﻿using System.Collections.Generic;

namespace webshop_v3.Models {
    public class ProduktListe {
        public string Beschreibung { get; set; }
        public List < Produkt > Produkte { get; set; }
        public List < Kategorie > Oberkategorien { get; set; }
        public List < Kategorie > Unterkategorien { get; set; }
        public List < Bild > Bilder { get; set; }

        public void Fill ( string queryString ) {
            Kategorie kategorie = null;
            var controller = new SqlController();
            var oberkategorien = controller.GetOberkategorien();
            var unterkategorien = controller.GetUnterkategorien();
            var bilder = controller.GetBilder();
            var produkte = controller.GetProdukte();

            if (queryString != null && (kategorie =
                    unterkategorien.Find(x => x.Id == int.Parse(queryString))) != null)
            {
                //ViewBag.SpeisenKat = kategorie.Bezeichnung;
                this.Beschreibung = kategorie.Bezeichnung;

                var kategorieId = int.Parse(queryString);
                var katProdukte = produkte.FindAll(x => x.Kategorie == kategorieId);
                //ViewBag.KatProdukte = katProdukte;
                this.Produkte = katProdukte;
            }
            else
            {
                this.Beschreibung = "Bestseller";
                //ViewBag.KatProdukte = produkte;
                this.Produkte = produkte;
            }

            //ViewBag.Oberkategorien = oberkategorien;
            //ViewBag.Unterkategorien = unterkategorien;
            //ViewBag.Bilder = bilder;
            this.Oberkategorien = oberkategorien;
            this.Unterkategorien = unterkategorien;
            this.Bilder = bilder;
        }
    }
}