﻿namespace webshop_v3.Models {
    public class User {
        public User ( int id , string alias , string email , bool active , string date , string vorname ,
            string nachname , string role ) {
            Id = id;
            Alias = alias;
            Email = email;
            Active = active;
            Date = date;
            Vorname = vorname;
            Nachname = nachname;
            Role = role;
        }

        public int Id { get; set; }
        public string Alias { get; set; }
        public string Email { get; set; }
        public bool Active { get; set; }
        public string Date { get; set; }
        public string Vorname { get; set; }
        public string Nachname { get; set; }
        public string Role { get; set; }
    }
}