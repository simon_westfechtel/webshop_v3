﻿namespace webshop_v3.Models {
    public class Produkt {
        public Produkt ( int id , string beschreibung , string name , int kategorie ) {
            Id = id;
            Beschreibung = beschreibung;
            Name = name;
            Kategorie = kategorie;
        }

        public int Id { get; set; }
        public string Beschreibung { get; set; }
        public string Name { get; set; }
        public int Kategorie { get; set; }

        public bool Vegetarisch () {
            return false;
        }

        public bool Vegan () {
            return false;
        }

        public override bool Equals ( object obj ) {
            if ( obj == null ) return false;
            var objAsProdukt = obj as Produkt;
            if ( objAsProdukt == null ) return false;
            return Equals ( objAsProdukt );
        }

        public override int GetHashCode () {
            return Id;
        }

        public bool Equals ( Produkt other ) {
            if ( other == null ) return false;
            return Id.Equals ( other.Id );
        }
    }
}