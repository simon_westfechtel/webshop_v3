﻿namespace webshop_v3.Models {
    public class DetailsListe {
        public Produkt Produkt { get; set; }
        public Bild Bild { get; set; }
        public double Preis { get; set; }
        public string PreisBezeichner { get; set; }

        public void Fill () {
            
        }
    }
}