﻿namespace webshop_v3.Models {
    public class Kategorie {
        public Kategorie ( int id , string bezeichnung , int oberkategorie /*, int produkt*/ ) {
            Id = id;
            Bezeichnung = bezeichnung;
            Oberkategorie = oberkategorie;
            //this.Produkt = produkt;
        }

        public int Id { get; set; }
        public string Bezeichnung { get; set; }

        public int Oberkategorie { get; set; }
        //public int Produkt { get; set; }
    }
}