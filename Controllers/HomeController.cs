﻿using System.Web.Mvc;
using webshop_v3.Models;

//using webshop_v3.Models;

namespace webshop_v3.Controllers {
    public class HomeController : Controller {
        // GET: Start
        public ActionResult Index () {
            XmlController xmlController = new XmlController ();
            Menu menu = xmlController.GetMenu ();

            return View (menu);
        }

        public ActionResult Produkt () {
            //Kategorie kategorie = null;
            //var produktListe = new ProduktListe ();
            //var controller = new SqlController ();
            //var oberkategorien = controller.GetOberkategorien ();
            //var unterkategorien = controller.GetUnterkategorien ();
            //var bilder = controller.GetBilder ();
            //var produkte = controller.GetProdukte ();


            //if ( Request.QueryString ["kategorie"] != null && ( kategorie =
            //         unterkategorien.Find ( x => x.Id == int.Parse ( Request.QueryString ["kategorie"] ) ) ) != null ) {
            //    //ViewBag.SpeisenKat = kategorie.Bezeichnung;
            //    produktListe.Beschreibung = kategorie.Bezeichnung;

            //    var kategorieId = int.Parse ( Request.QueryString ["kategorie"] );
            //    var katProdukte = produkte.FindAll ( x => x.Kategorie == kategorieId );
            //    //ViewBag.KatProdukte = katProdukte;
            //    produktListe.Produkte = katProdukte;
            //}
            //else {
            //    produktListe.Beschreibung = "Bestseller";
            //    //ViewBag.KatProdukte = produkte;
            //    produktListe.Produkte = produkte;
            //}

            ////ViewBag.Oberkategorien = oberkategorien;
            ////ViewBag.Unterkategorien = unterkategorien;
            ////ViewBag.Bilder = bilder;
            //produktListe.Oberkategorien = oberkategorien;
            //produktListe.Unterkategorien = unterkategorien;
            //produktListe.Bilder = bilder;
            ProduktListe produktListe = new ProduktListe ();
            produktListe.Fill ( Request.QueryString["kategorie"] );
            return View ( produktListe );
        }

        public ActionResult Impressum () {
            return View ();
        }

        public ActionResult Details ( int id = 0 ) {
            var detailsListe = new DetailsListe ();
            var controller = new SqlController ();
            var produkte = controller.GetProdukte ();
            var bilder = controller.GetBilder ();

            if (
                produkte.Find ( x => x.Id == id ) == null ) return RedirectToAction ( "Index" );

            var produkt = produkte.Find ( x => x.Id == id );
            var bild = bilder.Find ( x => x.Produkt == produkt.Id );
            var preis = controller.GetProduktPreis ( produkt.Id );

            detailsListe.Produkt = produkt;
            detailsListe.Bild = bild;

            if ( Session ["user"] == null ) {
                detailsListe.PreisBezeichner = "Gast-Preis";
                detailsListe.Preis = preis.Gastpreis;
            }
            else {
                var user = ( User ) Session ["user"];
                switch ( user.Role ) {
                    case "Gast":
                        detailsListe.PreisBezeichner = "Gast-Preis";
                        detailsListe.Preis = preis.Gastpreis;
                        break;
                    case "Student":
                        detailsListe.PreisBezeichner = "Studenten-Preis";
                        detailsListe.Preis = preis.Studentenpreis;
                        break;
                    case "Mitarbeiter":
                        detailsListe.PreisBezeichner = "Mitarbeiter-Preis";
                        detailsListe.Preis = preis.Mitarbeiterpreis;
                        break;
                }
            }

            return View ( detailsListe );
        }
    }
}