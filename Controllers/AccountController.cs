﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using webshop_v3.DataModels;
using webshop_v3.Models;

namespace webshop_v3.Controllers {
    public class AccountController : Controller {
        // GET: Account
        public ActionResult Login ( LoginListe loginListe = null ) {
            //LoginListe loginListe = new LoginListe ();

            if ( ( User ) Session ["user"] == null ) {
                // user nicht eingeloggt

                // noch keine fehlerhafte eingabe
                string usr , pass;
                if ( !string.IsNullOrEmpty ( usr = Request ["details_login_usr"] ) &&
                     !string.IsNullOrEmpty ( pass = Request ["details_login_pass"] ) ) {
                    // name & pass gesetzt
                    var sqlController = new SqlController ();
                    try {
                        var user = sqlController.LoginUser ( usr , pass ); // Versuche user einzuloggen
                        Session ["user"] = user; // Wenn erfolgreich, speichere user objekt in session
                        return RedirectToAction ( "UCP" );
                    }
                    catch ( BadLoginAttemptException e ) {
                        var l = new LoginListe ();
                        l.Fehlermeldung =
                            e.Message; // Wenn fehlgeschlagen, speichere fehlermeldung in session
                        return View ( l );
                    }
                }
                {
                    // Name oder Pass nicht gesetzt
                    //loginListe.Fehlermeldung = "Benutzername oder Passwort leer."; // Fehlermeldung speichern
                    var l = new LoginListe ();
                    l.Fehlermeldung = "Benutzername oder Passwort leer.";
                    return View ( l );
                }
            }
            return RedirectToAction ( "UCP" );
        }

        public ActionResult UCP () {
            if ( Session ["user"] == null ) return RedirectToAction ( "Login" );
            if ( ( ( User ) Session ["user"] ).Role == "BackendNutzer" ) {
                var db = new PraktikumDB ();
                IEnumerable < FeNutzer > loginQuery =
                    from user in db.FeNutzer
                    where user.Aktiv
                    orderby user.LetzterLogin descending
                    select user;
                IEnumerable < FeNutzer > registerQuery =
                    from user in db.FeNutzer
                    where !user.Aktiv
                    orderby user.Anlegedatum descending
                    select user;

                var resultList = new List < IEnumerable < FeNutzer > > ();
                resultList.Add ( loginQuery );
                resultList.Add ( registerQuery );

                return View ( resultList );
            }
            return View ();
        }

        public ActionResult Ausloggen () {
            Session ["user"] = null;
            var loginListe = new LoginListe ();
            loginListe.Fehlermeldung = "Sie wurden abgemeldet.";
            return View ( "Login" , loginListe );
        }

        public ActionResult Registrieren () {
            if ( Request ["register_type"] == null ) return View ();
            var sqlController = new SqlController ();
            var registrierenListe = new RegistrierenListe ();
            if ( sqlController.CheckUserNick ( Request ["register_nickname"] ) &&
                 sqlController.CheckUserEmail ( Request ["register_email"] ) )
                if ( Request ["register_type"] != null ) {
//nur zur sicherheit...

                    registrierenListe.Type = int.Parse ( Request ["register_type"] );
                    registrierenListe.Datum = DateTime.Today.ToString ( "yyyy-MM-dd" );
                    registrierenListe.Passwort = Request ["register_pw1"];
                    registrierenListe.Vorname = Request ["register_name"];
                    registrierenListe.Nachname = Request ["register_surname"];
                    registrierenListe.Grund = Request ["register_reason"];
                    registrierenListe.MitarbeiterNummer = Request ["register_staffnumber"];
                    registrierenListe.TelefonNummer = Request ["register_fon"];
                    registrierenListe.Buero = Request ["register_office"];
                    registrierenListe.MatrikelNummer = Request ["register_matr"];
                    registrierenListe.StudienGang = Request ["register_stud"];
                    return View ( "Registrieren_Beide" , registrierenListe );
                }
                else {
                    return View ();
                }
            if ( sqlController.CheckUserEmail ( Request ["register_email"] ) )
                if ( Request ["register_type"] != null ) {
                    //RegistrierenListe registrierenListe = new RegistrierenListe();
                    registrierenListe.Type = int.Parse ( Request ["register_type"] );
                    registrierenListe.Datum = DateTime.Today.ToString ( "yyyy-MM-dd" );
                    registrierenListe.Passwort = Request ["register_pw1"];
                    registrierenListe.Vorname = Request ["register_name"];
                    registrierenListe.Nachname = Request ["register_surname"];
                    registrierenListe.Grund = Request ["register_reason"];
                    registrierenListe.MitarbeiterNummer = Request ["register_staffnumber"];
                    registrierenListe.TelefonNummer = Request ["register_fon"];
                    registrierenListe.Buero = Request ["register_office"];
                    registrierenListe.MatrikelNummer = Request ["register_matr"];
                    registrierenListe.StudienGang = Request ["register_stud"];
                    registrierenListe.Loginname = Request ["register_nickname"];
                    return View ( "Registrieren_Email" , registrierenListe );
                }
                else {
                    return View ();
                }
            if ( sqlController.CheckUserNick ( Request ["register_nickname"] ) )
                if ( Request ["register_type"] != null ) {
                    //RegistrierenListe registrierenListe = new RegistrierenListe();
                    registrierenListe.Type = int.Parse ( Request ["register_type"] );
                    registrierenListe.Datum = DateTime.Today.ToString ( "yyyy-MM-dd" );
                    registrierenListe.Passwort = Request ["register_pw1"];
                    registrierenListe.Vorname = Request ["register_name"];
                    registrierenListe.Nachname = Request ["register_surname"];
                    registrierenListe.Grund = Request ["register_reason"];
                    registrierenListe.MitarbeiterNummer = Request ["register_staffnumber"];
                    registrierenListe.TelefonNummer = Request ["register_fon"];
                    registrierenListe.Buero = Request ["register_office"];
                    registrierenListe.MatrikelNummer = Request ["register_matr"];
                    registrierenListe.StudienGang = Request ["register_stud"];
                    registrierenListe.Email = Request ["register_email"];
                    return View ( "Registrieren_Nick" , registrierenListe );
                }
                else {
                    return View ();
                }
            registrierenListe.Type = int.Parse ( Request ["register_type"] );
            registrierenListe.Datum = DateTime.Today.ToString ( "yyyy-MM-dd" );
            registrierenListe.Passwort = Request ["register_pw1"];
            registrierenListe.Vorname = Request ["register_name"];
            registrierenListe.Nachname = Request ["register_surname"];
            registrierenListe.Grund = Request ["register_reason"];
            registrierenListe.MitarbeiterNummer = Request ["register_staffnumber"];
            registrierenListe.TelefonNummer = Request ["register_fon"];
            registrierenListe.Buero = Request ["register_office"];
            registrierenListe.MatrikelNummer = Request ["register_matr"];
            registrierenListe.StudienGang = Request ["register_stud"];
            registrierenListe.Email = Request ["register_email"];
            registrierenListe.Loginname = Request ["register_nickname"];
            sqlController.RegisterUser ( registrierenListe );
            return View ( "Registrieren_Erfolg" );
        }

        public ActionResult Registrieren_Beide ( RegistrierenListe registrierenListe ) {
            var sqlController = new SqlController ();
            if ( sqlController.CheckUserNick ( Request ["register_nickname"] ) &&
                 sqlController.CheckUserEmail ( Request ["register_email"] ) )
                return View ( "Registrieren_Beide" , registrierenListe );
            if ( sqlController.CheckUserEmail ( Request ["register_email"] ) ) {
                registrierenListe.Loginname = Request ["register_nickname"];
                return View ( "Registrieren_Email" , registrierenListe );
            }

            if ( sqlController.CheckUserNick ( Request ["register_nickname"] ) ) {
                registrierenListe.Loginname = Request ["register_email"];
                return View ( "Registrieren_Nick" , registrierenListe );
            }

            registrierenListe.Loginname = Request ["register_nickname"];
            registrierenListe.Email = Request ["register_email"];
            sqlController.RegisterUser ( registrierenListe );
            return View ( "Registrieren_Erfolg" );
        }

        public ActionResult Registrieren_Email ( RegistrierenListe registrierenListe ) {
            var sqlController = new SqlController ();
            if ( sqlController.CheckUserEmail ( Request ["register_email"] ) )
                return View ( "Registrieren_Email" , registrierenListe );
            registrierenListe.Email = Request ["register_email"];
            sqlController.RegisterUser ( registrierenListe );
            return View ( "Registrieren_Erfolg" );
        }

        public ActionResult Registrieren_Nick ( RegistrierenListe registrierenListe ) {
            var sqlController = new SqlController ();
            if ( sqlController.CheckUserNick ( Request ["register_nickname"] ) )
                return View ( "Registrieren_Nick" , registrierenListe );
            registrierenListe.Loginname = Request ["register_nickname"];
            sqlController.RegisterUser ( registrierenListe );
            return View ( "Registrieren_Erfolg" );
        }

        public ActionResult Registrieren_Erfolg () {
            return View ();
        }

        public ActionResult ActivateUser ( int id ) {
            SqlController sqlController = new SqlController ();
            sqlController.ActivateUser ( id );
            return RedirectToAction("UCP");
        }

        public ActionResult DeactivateUser(int id)
        {
            SqlController sqlController = new SqlController();
            sqlController.DeactivateUser(id);
            return RedirectToAction("UCP");
        }
    }
}